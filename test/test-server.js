process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require("../server/app");
var should = chai.should();
chai.use(chaiHttp);

describe("Unit Test", function () {
  //sum
  //test case 1
  it("API POST /sum  2 + 3 = 5", function (done) {
    chai
      .request(server)
      .post("/sum")
      .send({ x: 2, y: 3 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(5);
        done();
      });
  });

  //test case 2
  it("API POST /sum 2 + '3' = throw", function (done) {
    chai
      .request(server)
      .post("/sum")
      .send({ x: 2, y: "3" })
      .end(function (err, res) {
        res.should.have.status(500);
        res.should.be.throw;
        done();
      });
  });

  //test case 3
  it("API POST /sum  -2 + 2 = 0", function (done) {
    chai
      .request(server)
      .post("/sum")
      .send({ x: -2, y: 2 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(0);
        done();
      });
  });

  //subtraction
  //test case 4
  it("API POST /subtraction  -2 - 2 = -4", function (done) {
    chai
      .request(server)
      .post("/subtraction")
      .send({ x: -2, y: 2 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(-4);
        done();
      });
  });

  //test case 5
  it("API POST /subtraction  '6' - 2 = throw", function (done) {
    chai
      .request(server)
      .post("/subtraction")
      .send({ x: '6', y: 2 })
      .end(function (err, res) {
        res.should.have.status(500);
        res.should.be.throw;
        done();
      });
  });

  //test case 6
  it("API POST /subtraction  100 - 32 = 68", function (done) {
    chai
      .request(server)
      .post("/subtraction")
      .send({ x: 100, y: 32 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(68);
        done();
      });
  });
  
  //multiplication
  //test case 7
  it("API POST /multiplication  -2 * 2 = -4", function (done) {
    chai
      .request(server)
      .post("/multiplication")
      .send({ x: -2, y: 2 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(-4);
        done();
      });
  });

  //test case 8
  it("API POST /multiplication  '6' * 2 = throw", function (done) {
    chai
      .request(server)
      .post("/multiplication")
      .send({ x: '6', y: 2 })
      .end(function (err, res) {
        res.should.have.status(500);
        res.should.be.throw;
        done();
      });
  });

  //test case 9
  it("API POST /multiplication  -100 * -32 = 68", function (done) {
    chai
      .request(server)
      .post("/multiplication")
      .send({ x: -100, y: -32 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(3200);
        done();
      });
  });

  //division
  //test case 10
  it("API POST /division  -2 / 2 = -1", function (done) {
    chai
      .request(server)
      .post("/division")
      .send({ x: -2, y: 2 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(-1);
        done();
      });
  });

  //test case 11
  it("API POST /division  '6' / '2' = throw", function (done) {
    chai
      .request(server)
      .post("/division")
      .send({ x: '6', y: 2 })
      .end(function (err, res) {
        res.should.have.status(500);
        res.should.be.throw;
        done();
      });
  });

  //test case 12
  it("API POST /division  32 / 100 = 0.32", function (done) {
    chai
      .request(server)
      .post("/division")
      .send({ x: 32, y: 100 })
      .end(function (err, res) {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a("object");
        res.body.should.have.property("msg");
        res.body.msg.should.have.equal("OK");
        res.body.should.have.property("result");

        res.body.result.should.have.equal(0.32);
        done();
      });
  });
});
