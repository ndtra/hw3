#!/bin/bash
set -eux;

if [ $# -eq 0 ]; then
    echo "Path to deploy is missing"
    exit 1
else
    dir="$1"
fi

cd $dir

git fetch
git reset --hard
git checkout master
git pull

npm install 

npm test

sudo service hw3 restart